from __future__ import division
import gzip
import sys,os
import time,re
import subprocess

time1=time.time()
#######################################################################
###################### start basecall and fq2sam  #####################
#######################################################################

dir1='C007R022_basecall_D'   #####在指定目录下建立新文件夹
dir2='C007R022basecall_Dsrc_p'
cur_dir='Z:\LM\G_increase\SE120\PF'
if os.path.isdir(cur_dir):
    os.mkdir(os.path.join(cur_dir,dir1))
    os.mkdir(os.path.join(cur_dir,dir2))
######################调用basecall程序####################
cmd1="Z:\\LM\\Zebracall\\zebracall_online_v0.3.9.8199\\basecall\\basecall1.exe Z:\\LM\\G_increase\\SE120\\L01\\Rawimage\\L01 \
Z:\\LM\\G_increase\\SE120\\PF\\C007R022_basecall_D 120 7 22 -S -R -M -F"  #getint
cmd2="Z:\\LM\\liuerkai\\basecall\\basecall_I_SRC_P_8199.exe Z:\\LM\\G_increase\\SE120\\L01\\Rawimage\\L01 \
Z:\\LM\\G_increase\\SE120\\PF\\C007R022basecall_Dsrc_p 120 7 22 -S -R -M -F -C"  #getIntsSrc,coordinate
cmd3="python C:\\Users\\xudongyang\\Desktop\\offlineMapping\\fq2sam.py Z:\\LM\\G_increase\\SE120\\PF\\C007R022_basecall_D\\B1234567_L02_read.fq.gz \
B1234567_L02 Z:\\LM\\G_increase\\SE120\\PF\\C007R022_basecall_D"  #fq2sam
subprocess.Popen(cmd2)   ###设置子进程并行运行，加快速度
time.sleep(10)
os.system(cmd1)
os.system(cmd3)
print time.time()-time1

#######################################################################
###################### analysise by choosed cycle  ####################
#######################################################################
cycle_mum='S80'
cyc=80

intSrc="Z:\\LM\\Zebracall\\bin2txt\\bin2txt_12.11.exe D:\\LM\\TE120\\C007R022basecall_Dsrc_p\\Intensities\\rawInts\\%s\\C007R022.bin \
Z:\\LM\\G_increase\\SE120\\PF\\C007R022basecall_Dsrc_p\\Intensities\\rawInts\\%s\\C007R022.txt"%(cycle_mum,cycle_mum)
rawint="Z:\\LM\\Zebracall\\bin2txt\\bin2txt_12.11.exe D:\\LM\\TE120\\C007R022_basecall_D\\Intensities\\rawInts\\%s\\C007R022.bin \
Z:\\LM\\G_increase\\SE120\\PF\\C007R022_basecall_D\\Intensities\\rawInts\\%s\\C007R022.txt"%(cycle_mum,cycle_mum)
midint="Z:\\LM\\Zebracall\\bin2txt\\bin2txt_12.11.exe D:\\LM\\TE120\\C007R022_basecall_D\\Intensities\\midInts\\%s\\C007R022.bin \
Z:\\LM\\G_increase\\SE120\\PF\\C007R022_basecall_D\\Intensities\\midInts\\%s\\C007R022.txt"%(cycle_mum,cycle_mum)
finint="Z:\\LM\\Zebracall\\bin2txt\\bin2txt_12.11.exe D:\\LM\\TE120\\C007R022_basecall_D\\Intensities\\finInts\\%s\\C007R022.bin \
Z:\\LM\\G_increase\\SE120\\PF\\C007R022_basecall_D\\Intensities\\finInts\\%s\\C007R022.txt"%(cycle_mum,cycle_mum)  #bin2txt
os.system(intSrc)
cmd_list=[rawint,midint,finint]
for i in cmd_list:
    subprocess.Popen(i)
    
time.sleep(10)
################ statistic data prepare###############
intsr=open(r'Z:\LM\G_increase\SE120\PF\C007R022basecall_Dsrc_p\Intensities\rawInts\%s\C007R022.txt'%cycle_mum)
t1=intsr.readlines()
for i in range(len(t1)):
    t1[i]=t1[i].split()
intsr.close()

dic=dict([[0,'A'],[1,'C'],[2,'G'],[3,'T']])    ###建立字典方便找出各int所call出的碱基
raw=open(r'Z:\LM\G_increase\SE120\PF\C007R022_basecall_D\Intensities\rawInts\%s\C007R022.txt'%cycle_mum)
t2=raw.readlines()
for i in range(len(t2)):
    t2[i]=t2[i].split()
GRR_r=[]
for i in range(len(t2)):
    fourint=[]
    GRR=[]
    for j in range(4):
        fourint.append(float(t2[i][j]))
    call_base=dic[fourint.index(max(fourint))]     #由rawint所call出的base
    fourint.sort()
    if fourint[3]<=0:
        GRR.append(0)
    elif fourint[3]>0 and fourint[2]<=0:
        GRR.append(1)
    else:
        GRR.append('%.4f'%(fourint[3]/(fourint[3]+fourint[2])))
    GRR.append(call_base)
    GRR_r.append(GRR)
raw.close()

mid=open(r'Z:\LM\G_increase\SE120\PF\C007R022_basecall_D\Intensities\midInts\%s\C007R022.txt'%cycle_mum)
t3=mid.readlines()
for i in range(len(t3)):
    t3[i]=t3[i].split()
GRR_m=[]
for i in range(len(t3)):
    fourint=[]
    GRR=[]
    for j in range(4):
        fourint.append(float(t3[i][j]))
    call_base=dic[fourint.index(max(fourint))]
    fourint.sort()
    if fourint[3]<=0:
        GRR.append(0)
    elif fourint[3]>0 and fourint[2]<=0:
        GRR.append(1)
    else:
        GRR.append('%.4f'%(fourint[3]/(fourint[3]+fourint[2])))
    GRR.append(call_base)
    GRR_m.append(GRR)
mid.close()

fin=open(r'Z:\LM\G_increase\SE120\PF\C007R022_basecall_D\Intensities\finInts\%s\C007R022.txt'%cycle_mum)
t4=fin.readlines()
for i in range(len(t4)):
    t4[i]=t4[i].split()
GRR_f=[]
for i in range(len(t4)):
    fourint=[]
    for j in range(4):
        fourint.append(float(t4[i][j]))
    fourint.sort()
    if fourint[3]<=0:
        GRR_f.append(0)
    elif fourint[3]>0 and fourint[2]<=0:
        GRR_f.append(1)
    else:
        GRR_f.append('%.4f'%(fourint[3]/(fourint[3]+fourint[2])))
fin.close()

fq=gzip.open(r'Z:\LM\G_increase\SE120\PF\C007R022_basecall_D\B1234567_L02_read.fq.gz')
base_list=[]
t=fq.readlines()
for i in range(1,len(t),4):
    base_list.append(t[i].split()[0][cyc-1])   #根据cycle号变动
fq.close()
##########################################################################
########################## get refbase ###################################
##########################################################################
f=open(r'Z:\LM\G_increase\SE120\PF\C007R022_basecall_D\B1234567_L02.sam','rb')
r0=f.readline()
r0=f.readline()
r=f.readlines()
for i in range(len(r)):
    r[i]=r[i].split()
ref_base=[]
cycle_num=[]
#cyc=114               ######cycle number input
oppo=dict([['A','T'],['C','G'],['G','C'],['T','A']])
for i in range(len(r)):
    if int(r[i][1])==16 and r[i][5]=='120M':
        if r[i][18][5:]=='120':
            cycle_num.append(i)
            ref_base.append(oppo[r[i][9][-cyc]])
        else:
            cycle_num.append(i)
            tem=r[i][18][5:]
            m_num=re.findall(r"\d+\.?\d*",tem)
            base=re.findall(r"\D",tem)
            s_m=[]
            m_num.reverse()
            base.reverse()
            for j in range(len(m_num)):
                m_num[j]=int(m_num[j])
            for k in range(len(base)):
                s_m.append([sum(m_num[:k+1])+k+1,base[k]])
            ddic=dict(s_m)
            if cyc in ddic:
                ref_base.append(oppo[ddic[cyc]])
            else:
                ref_base.append(oppo[r[i][9][-cyc]])
    elif int(r[i][1])==4:
        cycle_num.append(i)
        ref_base.append('N')
    elif int(r[i][1])==0 and r[i][5]=='120M':
        if r[i][18][5:]=='120':
            cycle_num.append(i)
            ref_base.append(r[i][9][cyc-1])
        else:
            cycle_num.append(i)
            tem=r[i][18][5:]
            m_num=re.findall(r"\d+\.?\d*",tem)
            base=re.findall(r"\D",tem)
            s_m=[]
            for j in range(len(m_num)):
                m_num[j]=int(m_num[j])
            for k in range(len(base)):
                s_m.append([sum(m_num[:k+1])+k+1,base[k]])
            ddic=dict(s_m)
            if cyc in ddic:
                ref_base.append(ddic[cyc])
            else:
                ref_base.append(r[i][9][cyc-1])


############generate result file#######################
f2=open('C007R022_cycle%d_stat.txt'%cyc,'a+')
j=0
for i in cycle_num:
    print >>f2,str(i).ljust(7),base_list[i],ref_base[j],GRR_f[i],'\t',GRR_r[i][0],GRR_r[i][1],'\t',GRR_m[i][0],GRR_m[i][1],'\t',(t2[i][0][:8]).center(9),\
          (t2[i][1][:8]).center(9),(t2[i][2][:8]).center(9),(t2[i][3][:8]).center(9),'\t',(t3[i][0][:8]).center(9),(t3[i][1][:8]).center(9),(t3[i][2][:8]).center(9),\
          (t3[i][3][:8]).center(9),'\t',(t4[i][0][:8]).center(9),(t4[i][1][:8]).center(9),(t4[i][2][:8]).center(9),(t4[i][3][:8]).center(9),'\t',(t1[i][0][:8]).center(9),\
          (t1[i][1][:8]).center(9),(t1[i][2][:8]).center(9),(t1[i][3][:8]).center(9)
    j+=1
          
   
f2.close()

time2=time.time()
print time2-time1
base_list=[];ref_base=[]
GRR_r=[];GRR_m=[];GRR_f=[]
t1=[];t2=[];t3=[];t4=[]

