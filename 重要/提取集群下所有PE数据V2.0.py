import sys,os
import re,time
################################################################################################
##################################有定义函数调用，便于读写#########################################
################################################################################################
start_time = time.time()
data_path = '/share/Zebra/05.SeqData/'
item_list = ['machine#','Slide','Lane','Time','TotalReads(M)','CycleNumber','len_read1','len_read2','Lag1','Lag2','GRR','read1_fcyc_intA',\
        'read1_fcyc_intC','read1_fcyc_intT','read1_fcyc_intG','read1_lcyc_intA','read1_lcyc_intC','read1_lcyc_intT','read1_lcyc_intG','read2_fcyc_intA',\
        'read2_fcyc_intC','read2_fcyc_intG','read2_fcyc_intG','read2_lcyc_intA','read2_lcyc_intC','read2_lcyc_intT','read2_lcyc_intG','\n']
fo = open('statPE_fromHTML.csv','w+')
fo.write(','.join(item_list))

def ReadHtml(path):
    fov_number = 0
    cycle_number = 0
    totalreads = 0
    time = 0
    GRR = 0
    Lag1 = 0
    Lag2 = 0
    read1 = 0
    read2 = 0
    A = [];C = [];T = [];G = []
    with open(path[1],'r') as fh_in:
        for line in fh_in:
            if re.search('var fovData',line):
                fov_number = line.count(': {')
    with open(path[0],'r') as fh_in:
        for line in fh_in:
            if re.search('var fqTable =',line):
                re1=re.compile('read\s1\'\,\s(.*)\]\,\s\[\'read\s2').findall(line)
                re2=re.compile('read\s2\'\,\s(.*)\]\,\s\[\'read\stotal').findall(line)
                read1 = int(re1[0].split(',')[2].strip('\' '))/int(re1[0].split(',')[1].strip('\' '))
                read2 = int(re2[0].split(',')[2].strip('\' '))/int(re2[0].split(',')[1].strip('\' '))
            if re.search('var reportTime',line):
                time = line.split('"')[1]
            if re.search('var summaryTable',line):
                totalreads = re.compile('TotalReads\(M\)\'\,\s(.*)\]\,\s\[\'MappedReads').findall(line)[0]
                cyc = re.compile('CycleNumber\'\,\s([0-9]{2,3})\]\,\s\[\'').findall(line)
                if len(cyc)==0:
                    cycle_number = 'NULL'
                else:
                    cycle_number = cyc[0]
                lag1 = re.compile('Lag1\%\'\,\s(.*)\]\,\s\[\'Lag2').findall(line)
                if len(lag1) == 0:
                    Lag1 = 'NULL'
                else:
                    Lag1 = lag1[0]
                lag2 = re.compile('Lag2\%\'\,\s(.*)\]\,\s\[\'Runon1').findall(line)
                if len(lag2) == 0:
                    Lag2 = 'NULL'
                else:
                    Lag2 = lag2[0]
                grr = re.compile('GRR\%\'\,\s(.*)\]\,\s\[\'MappingRate').findall(line)
                if len(grr) == 0:
                    GRR = 'NULL'
                else:
                    GRR = grr[0]
            if re.search('var signal =',line):
                A=re.compile('A\"\:\s\[(.*)\]\,\s\"C').findall(line)
                if len(A) == 1:
                    A = A[0].split(',')
                else:
                    A = ['null' for i in range(150)]
                C=re.compile('C\"\:\s\[(.*)\]\,\s\"T').findall(line)
                if len(C) == 1:
                    C = C[0].split(',')
                else:
                    C = ['null' for i in range(150)]
                T=re.compile('T\"\:\s\[(.*)\]\,\s\"G').findall(line)
                if len(T) == 1:
                    T = T[0].split(',')
                else:
                    T = ['null' for i in range(150)]
                G=re.compile('G\"\:\s\[(.*)\]\}').findall(line)
                if len(G) == 1:
                    G = G[0].split(',')
                else:
                    G = ['null' for i in range(150)]
    item_list=[path[2],path[3],path[4],time,totalreads,str(fov_number),cycle_number,str(read1),str(read2),Lag1,Lag2,GRR,A[0],C[0],T[0],G[0]]
    int_list = [A[read1-1],C[read1-1],T[read1-1],G[read1-1],A[read1],C[read1],T[read1],G[read1],A[read1+read2-1],C[read1+read2-1],\
            T[read1+read2-1],G[read1+read2-1],'\n']
    item_list.extend(int_list)
    fo.write(','.join(item_list))

machine_list = os.listdir(data_path)
for i in machine_list:
    if i[:5] != 'Zebra':
        machine_list.remove(i)
for machine in machine_list:
    file_path = []
    slide_list = os.listdir('%s%s/'%(data_path,machine))
    for k in slide_list:
        if (k.count('.')+k.count('_')) > 0:
            slide_list.remove(k)
    for slide in slide_list:
        for lane in ['L01','L02']:
            summary = '%s%s/%s/%s/%s_%s.summaryReport.html'%(data_path,machine,slide,lane,slide,lane)
            heatmap = '%s%s/%s/%s/%s_%s.heatmapReport.html'%(data_path,machine,slide,lane,slide,lane)
            if os.path.exists(summary) and os.path.exists(heatmap):
                file_path.append([summary,heatmap,machine,slide,lane])
    lis=[]
    for i in range(len(file_path)):
        with open(file_path[i][0],'r') as fh_in:
            for line in fh_in:
                if re.search('var numCountPE = {"GapR1"',line):
                    lis.append(file_path[i])
    file_path = lis
    for path in file_path:
        ReadHtml(path)
fo.close()
print time.time()-start_time
        
    
