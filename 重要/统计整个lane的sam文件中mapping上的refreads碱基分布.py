from __future__ import division
import sys,os,re
import time
#############################################################################################################
########################## 统计一个lane的sam文件获得mapping上的reads的refreads的碱基分布数据########################
#############################################################################################################
time1=time.time()
fo=open('newref_reads.txt','a+')
oppo=dict([['A','T'],['C','G'],['G','C'],['T','A']])    ##构建互补字典
for line in open('/prod/pv-01/research/st_read/F13ZOOYJSY1380/xudongyang/LM/ref_base/603HW0614_L01_read.sam'):
    li=line.split()
    if li[0][:3]=='603':    #这是要滤掉sam文件前2行的注释行
        #print 1   
        if li[1]!='4':      #滤掉没有mapping上的
            #print 1
            if li[5]=='100M' and li[18][5:]=='100':    #对无mismatch全匹配得的处理
                #print li[0]
                if li[1]=='0':
                    print >>fo,li[9]
                elif li[1]=='16':
                    lis=list(li[9])
                    lis.reverse()
                    for i in range(100):
                        lis[i]=oppo[lis[i]]
                    print >>fo,''.join(lis)
            elif li[5]=='100M' and li[18][5:]!='100':  #对reads中有错配的情况的处理
                read=li[9]
                tem=li[18][5:]
                m_num=re.findall(r"\d+\.?\d*",tem)
                base=re.findall(r"\D",tem)
                s_m=[]
                for j in range(len(m_num)):
                    m_num[j]=int(m_num[j])
                for k in range(len(base)):
                    s_m.append([sum(m_num[:k+1])+k+1,base[k]])   #构建错配位点及对应ref碱基的字典
                ddic=dict(s_m)
                lis=list(read)
                for x in ddic:    
                    lis[x-1]=ddic[x]
                if li[1]=='0':
                    print >>fo,''.join(lis)
                elif li[1]=='16':
                    lis.reverse()
                    for j in range(100):
                        lis[j]=oppo[lis[j]]
                    print >>fo,''.join(lis)
fo.close()
print time.time()-time1

fa=open('arate.txt','a+')
fc=open('crate.txt','a+')
fg=open('grate.txt','a+')
ft=open('trate.txt','a+')
for i in range(100):
    A=0;C=0;G=0;T=0
    co=0
    for line in open('/prod/pv-01/research/st_read/F13ZOOYJSY1380/xudongyang/LM/newref_base/ref_reads.txt'):
        co+=1
        if line[i]=='A':
            A+=1
        elif line[i]=='C':
            C+=1
        elif line[i]=='G':
            G+=1
        elif line[i]=='T':
            T+=1
    print >>fa,'%.4f'%((A/co)*100)
    print >>fc,'%.4f'%((C/co)*100)
    print >>fg,'%.4f'%((G/co)*100)
    print >>ft,'%.4f'%((T/co)*100)
    print i+1,time.time()-time1

fa.close()
fc.close()
fg.close()
ft.close()
print time.time()-time1

