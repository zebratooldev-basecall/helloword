from __future__ import division
import gzip
import sys,os
import time,re

cycle = 'S090'

nor1 = []
raw = []
fq = []
read_num = 0
ref_base = []
cycle_num = []
dic=dict([[0,'A'],[1,'C'],[2,'G'],[3,'T']])

fr = open(r'Z:\LM\G_increase\8199\8199_tst_SE120\inside_block\normal\Intensities\rawInts\%s\C007R022.txt'%cycle,'rb')
t1 = fr.readlines()
for i in range(len(t1)):
    t1[i] = t1[i].split()
    four = [float(j) for j in t1[i]]
    raw.append(dic[four.index(max(four))])
fr.close()
#t1 = []
#tt = []
fn = open(r'Z:\LM\G_increase\8199\8199_tst_SE120\inside_block\normal\Intensities\rawInts\%s\nor1Int.txt'%cycle,'rb')
t2 = fn.readlines()                                                                                                                                                                     
for i in range(len(t2)):
    t2[i] = t2[i].split()
    four = [float(j) for j in t2[i]]
    #tt.append([str(k) for k in four])
    nor1.append(dic[four.index(max(four))])
fn.close()
t2 = []

f3 = gzip.open(r'Z:\LM\G_increase\8199\8199_tst_SE120\inside_block\normal\B1234567_L02_read.fq.gz')
t3=f3.readlines()
for i in range(1,len(t3),4):
    fq.append(t3[i].split()[0][int(cycle[1:])-1])   
f3.close()                                      
t3= 0
oppo=dict([['A','T'],['C','G'],['G','C'],['T','A']])
for i in open(r'Z:\LM\G_increase\8199\8199_tst_SE120\inside_block\normal\Intensities\FQMAP\B1234567_L02_read.sam','rb'):
    x=i.split()
    if x[0][:3]=='B12':   #去掉头两行
        read_num+=1
        if int(x[1])==16 and x[5]=='120M': 
            if x[18][5:]=='120':             #反向互补无call错
                cycle_num.append(read_num-1)
                ref_base.append(oppo[x[9][-int(cycle[1:])]])
            else:
                cycle_num.append(read_num-1)   #有call错
                tem=x[18][5:]
                m_num=re.findall(r"\d+\.?\d*",tem)
                base=re.findall(r"\D",tem)
                s_m=[]
                m_num.reverse()
                base.reverse()
                for j in range(len(m_num)):
                    m_num[j]=int(m_num[j])
                for k in range(len(base)):
                    s_m.append([sum(m_num[:k+1])+k+1,base[k]])
                ddic=dict(s_m)
                if int(cycle[1:]) in ddic:
                    ref_base.append(oppo[ddic[int(cycle[1:])]])
                else:
                    ref_base.append(oppo[x[9][-int(cycle[1:])]])
        elif int(x[1])==4:
            cycle_num.append(read_num-1)
            ref_base.append('N')
        elif int(x[1])==0 and x[5]=='120M':
            if x[18][5:]=='120':
                cycle_num.append(read_num-1)
                ref_base.append(x[9][int(cycle[1:])-1])
            else:
                cycle_num.append(read_num-1)
                tem=x[18][5:]
                m_num=re.findall(r"\d+\.?\d*",tem)
                base=re.findall(r"\D",tem)
                s_m=[]
                for j in range(len(m_num)):
                    m_num[j]=int(m_num[j])
                for k in range(len(base)):
                    s_m.append([sum(m_num[:k+1])+k+1,base[k]])
                ddic=dict(s_m)
                if int(cycle[1:]) in ddic:
                    ref_base.append(ddic[int(cycle[1:])])
                else:
                    ref_base.append(x[9][int(cycle[1:])-1])

fo = open('%sstat.txt'%cycle,'a+')
j = 0
for i in cycle_num:
    print >>fo,str(i),fq[i],ref_base[j],raw[i],nor1[i]
          
    j+=1
fo.close()

fn = open('%ssplit.csv'%cycle,'w+')
fn.write('ID,fqbase,refbase,rawCall,nor1Call,\n')
for line in open(r'Z:\xudongyang\S090\S090stat.txt','rb'):
    line = line.split()
    #print len(line)
    if line[2] != 'N' and line[3] != line[4]:
        fn.write(','.join(line))
        fn.write('\n')
fn.close()
print 'down'
        
    
