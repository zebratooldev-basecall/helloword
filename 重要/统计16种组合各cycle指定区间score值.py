from __future__ import division
import numpy,os,time
##############################################################################################
################## 对各组合的score值排序然后取指定区间的平均值（各对应cycle的） #####################
##############################################################################################
start = time.time()
fqlist = []
f = open(r'Z:\LM\G_increase\8199\8199_tst_SE120\8199_c007r022\L01\C007R022_NOPF_L01_read.fq')
t = f.readlines()
for i in range(1,len(t),4):
    fqlist.append(t[i])
t=[]
f.close()

ave_list = [[] for i in range(16)]    #存放所有数据3层嵌套列表
for i in range(16):
    for j in range(5):
        ave_list[i].append([])
nl = ['AA','AC','AG','AT','CA','CC','CG','CT','GA','GC','GG','GT','TA','TC','TG','TT']

def Q30_stat(cycle):
    #num_list = [0 for i in range(16)]
    ave = [[] for i in range(16)]      #存放当前cycle的80个值，每个子列表存5个值
    type_list = [['AA',0],['AC',0],['AG',0],['AT',0],['CA',0],['CC',0],['CG',0],['CT',0],\
             ['GA',0],['GC',0],['GG',0],['GT',0],['TA',0],['TC',0],['TG',0],['TT',0]]
    type_list1 = [['AA',[]],['AC',[]],['AG',[]],['AT',[]],['CA',[]],['CC',[]],['CG',[]],['CT',[]],\
             ['GA',[]],['GC',[]],['GG',[]],['GT',[]],['TA',[]],['TC',[]],['TG',[]],['TT',[]]]
    num = dict(type_list)       #建立字典存放各type的number数
    score = dict(type_list1)    #用字典对应的列表存放所有score值
    int_list = []
    f2 = open(r'Z:\LM\G_increase\8199\8199_tst_SE120\8199_c007r022\L01\Intensities\finInts\%s\C007R022.txt'%cycle[0])
    t2 = f2.readlines()
    for i in range(len(t2)):
        t2[i] = t2[i].split()
        four = []
        for j in range(4):
            four.append(float(t2[i][j]))
        int_list.append(four)
    for i in range(len(t2)):
        x = fqlist[i][(cycle[1]-2):cycle[1]]   #从fq截取2bp，开始计算
        if x in num:
            num[x] += 1
            temp = int_list[i]
            for j in range(4):
                if temp[j] < 0:
                    temp[j] = 0
            sc = max(temp)/sum(temp)
            if sc >= 0.8125:                #这里加上if语句就可以截取score值大于30的部分，不加则截取整体的score。
                score[x].append(sc)
    int_list = []
    for i in range(16):
        score[nl[i]].sort()
        tmd = score[nl[i]]
        l = len(tmd)
        m1 = numpy.mean(tmd[int(0.075*l):int(0.125*l)])  #求均值的函数mean（）需要从numpy模块调用
        m2 = numpy.mean(tmd[int(0.225*l):int(0.275*l)])
        m3 = numpy.mean(tmd[int(0.475*l):int(0.525*l)])
        m4 = numpy.mean(tmd[int(0.725*l):int(0.775*l)])
        m5 = numpy.mean(tmd[int(0.875*l):int(0.925*l)])
        lis = [m1,m2,m3,m4,m5]
        for j in range(5):
            sco = '%.2f'%(40*(lis[j]-0.25)/0.75)
            ave_list[i][j].append(sco)

cycle = []
cycle_list = ['S00%d'%i for i in range(2,10)]
cycle_list.extend(['S0%d'%i for i in range(10,100)])
cycle_list.extend(['S%d'%i for i in range(100,121)])
for i in range(2,121):
    cycle.append([cycle_list[i-2],i])            
for cycle in cycle:
    Q30_stat(cycle)

fo = open('wholeCycleQ30statupper30.csv','w+')
fo.write('cycle,')
for i in range(2,121):
    fo.write('%d,'%i)
fo.write('\n')

for i in range(16):
    temp1 = [nl[i]+'10%']   #这部分暂时似乎不能用循环来简化行数。
    temp2 = [nl[i]+'25%']
    temp3 = [nl[i]+'50%']
    temp4 = [nl[i]+'75%']
    temp5 = [nl[i]+'90%']
    temp1.extend(ave_list[i][0])
    temp2.extend(ave_list[i][1])
    temp3.extend(ave_list[i][2])
    temp4.extend(ave_list[i][3])
    temp5.extend(ave_list[i][4])
    fo.write(','.join(temp1))
    fo.write('\n')
    fo.write(','.join(temp2))
    fo.write('\n')
    fo.write(','.join(temp3))
    fo.write('\n')
    fo.write(','.join(temp4))
    fo.write('\n')
    fo.write(','.join(temp5))
    fo.write('\n\n')
fo.close()
print time.time()-start
    
