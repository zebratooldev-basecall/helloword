import sys,os
import re,time,json
######################################################################################
#########################改进：用json来处理特定字符串 ###############################
####################################################################################
start_time = time.time()
data_path = '/share/Zebra/05.SeqData/'
item_list = ['machine#','Slide','Lane','Time','TotalReads(M)','TotalFov','CycleNumber','MappingRate%','AvgErrorRate%','>Q30%','len_read1','len_read2','Lag1','Lag2','GRR','read1_fcyc_intA',\
        'read1_fcyc_intC','read1_fcyc_intT','read1_fcyc_intG','read1_lcyc_intA','read1_lcyc_intC','read1_lcyc_intT','read1_lcyc_intG','read2_fcyc_intA',\
        'read2_fcyc_intC','read2_fcyc_intG','read2_fcyc_intG','read2_lcyc_intA','read2_lcyc_intC','read2_lcyc_intT','read2_lcyc_intG','\n']
fo = open('statPE_fromHTML.csv','w+')
fo.write(','.join(item_list))

def ReadHtml(path):
    fov_number = 0
    cycle_number = 0
    totalreads = 0
    time = 0
    GRR = 0
    Lag1 = 0
    Lag2 = 0
    read1 = 0
    read2 = 0
    Q30 = 0
    mappingrate = 0
    errortare = 0
    A = [];C = [];T = [];G = []
    with open(path[1],'r') as fh_in:
        for line in fh_in:
            if re.search('var fovData',line):
                #fov_number = line.count(': {')
                s = re.compile('Data\s\=\s(.*)\;').findall(line)[0]
                temp_list = json.loads(s)
                fov_number = str(len(temp_list))
    with open(path[0],'r') as fh_in:
        for line in fh_in:
            if re.search('var fqTable =',line):
                s = re.compile('Table\s\=\s(.*)\;').findall(line)[0]
                t = s.replace('\'','\"')
                temp_list = json.loads(t)
                read1 = int(temp_list[1][3])/int(temp_list[1][2])
                read2 = int(temp_list[2][3])/int(temp_list[2][2])
                
            if re.search('var reportTime',line):
                time = line.split('"')[1]
            if re.search('var summaryTable',line):
                s = re.compile('Table\s\=\s(.*)\;').findall(line)[0]
                t = s.replace('\'','\"')
                temp_list = json.loads(t)
                dic = dict(temp_list)
                if 'TotalReads(M)' in dic:
                    totalreads = str(dic['TotalReads(M)'])
                else:
                    totalreads = 'null'
                if 'MappingRate%' in dic:
                    mappingrate = str(dic['MappingRate%'])
                else:
                    mappingrate = 'null'
                if '>Q30%' in  dic:
                    Q30 = str(dic['>Q30%'])
                else:
                    Q30 = 'null'
                if 'AvgErrorRate%' in dic:
                    errortare = str(dic['AvgErrorRate%'])
                else:
                    errortare = 'null'
                if 'CycleNumber' in dic:
                    cycle_number = str(dic['CycleNumber'])
                else:
                    cycle_number = 'null'

                if 'Lag1%' in dic:
                    Lag1 = str(dic['Lag1%'])
                else:
                    Lag1 = 'null'
                if 'Lad2%' in dic:
                    Lag2 = str(dic['Lag2%'])
                else:
                    Lag2 = 'null'
                if 'GRR%' in dic:
                    GRR = str(dic['GRR%'])
                else:
                    GRR = 'null'

            if re.search('var signal =',line):
                A=re.compile('A\"\:\s\[(.*)\]\,\s\"C').findall(line)
                if len(A) == 1:
                    A = A[0].split(',')
                else:
                    A = ['null' for i in range(150)]
                C=re.compile('C\"\:\s\[(.*)\]\,\s\"T').findall(line)
                if len(C) == 1:
                    C = C[0].split(',')
                else:
                    C = ['null' for i in range(150)]
                T=re.compile('T\"\:\s\[(.*)\]\,\s\"G').findall(line)
                if len(T) == 1:
                    T = T[0].split(',')
                else:
                    T = ['null' for i in range(150)]
                G=re.compile('G\"\:\s\[(.*)\]\}').findall(line)
                if len(G) == 1:
                    G = G[0].split(',')
                else:
                    G = ['null' for i in range(150)]

    item_list=[path[2],path[3],path[4],time,totalreads,fov_number,cycle_number,mappingrate,errortare,Q30,str(read1),str(read2),Lag1,Lag2,GRR,A[0],C[0],T[0],G[0]]
    int_list = [A[read1-1],C[read1-1],T[read1-1],G[read1-1],A[read1],C[read1],T[read1],G[read1],A[read1+read2-1],C[read1+read2-1],\
            T[read1+read2-1],G[read1+read2-1],'\n']
    item_list.extend(int_list)
    fo.write(','.join(item_list))

machine_list = os.listdir(data_path)
for i in machine_list:
    if i[:5] != 'Zebra':
        machine_list.remove(i)
#ok
for machine in machine_list:
    file_path = []
    slide_list = os.listdir('%s%s/'%(data_path,machine))
    for k in slide_list:
        if (k.count('.')+k.count('_')) > 0:
            slide_list.remove(k)
    for slide in slide_list:
        for lane in ['L01','L02']:
            summary = '%s%s/%s/%s/%s_%s.summaryReport.html'%(data_path,machine,slide,lane,slide,lane)
            heatmap = '%s%s/%s/%s/%s_%s.heatmapReport.html'%(data_path,machine,slide,lane,slide,lane)
            if os.path.exists(summary) and os.path.exists(heatmap):
                file_path.append([summary,heatmap,machine,slide,lane])
    lis=[]
    for i in range(len(file_path)):
        with open(file_path[i][0],'r') as fh_in:
            for line in fh_in:
                if re.search('var numCountPE = {"GapR1"',line):
                    lis.append(file_path[i])
    file_path = lis
    for path in file_path:
        ReadHtml(path)
fo.close()
print time.time()-start_time
        
    
