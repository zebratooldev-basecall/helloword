import os,sys
import re,time
t1=time.time()
##############################################################################################
################## 这里没有定义函数，嵌套太多不便读写移植但速度较快 #################################
##############################################################################################
machine=['Zebra00','Zebra06','Zebra08','Zebra10','Zebra11','Zebra13','Zebra17','Zebra20','Zebra21','Zebra25',\
         'Zebra30','Zebra36','Zebra38','Zebra42','Zebra43','Zebra44']      ##machinelist
fo=open('PEstat.csv','a+')
print >>fo,'machine#',',','Slide',',','Lane',',','Time',',','TotalReads(M)',',','TotalFov',',','CycleNumber',',','len_read1',',','len_read2',',','Lag1%',',','Lag2%',',',\
      'GRR%',',','read1_fcyc_intA',',','read1_fcyc_intC',',','read1_fcyc_intT',',','read1_fcyc_intG',',','read1_lcyc_intA',',','read1_lcyc_intC',\
      ',','read1_lcyc_intT',',','read1_lcyc_intG',',','read2_fcyc_intA',',','read2_fcyc_intC',',','read2_fcyc_intT',',','read2_fcyc_intG',\
      ',','read2_lcyc_intA',',','read2_lcyc_intC',',','read2_lcyc_intT',',','read2_lcyc_intG',','
for m in machine:
    m=m
    lis=[]      #将机器下的所有潜在路径存入列表
    path='/share/Zebra/05.SeqData/%s/'%m
    dirs=os.listdir(path)
    for i in dirs:
        if len(i.split('.')[0].split('_'))==1 and len(i)<17:   #过滤一些无关目录
            lis.append(i)
    for j in lis:
        if len(j)<4:
            lis.remove(j)
    lane=['L01','L02']
    for i in lis:
        sli=i
        for j in lane:
            lan=j
            x=path+sli+'/%s/'%lan+'%s_%s.summaryReport.html'%(sli,lan)  #建立文件的绝对路径
            y=path+sli+'/%s/'%lan+'%s_%s.heatmapReport.html'%(sli,lan)
            if os.path.exists(x) and os.path.exists(y):                 #判断绝对路径是否存在，true则表明有相应的HTML文件
                csv_lis=[]                                              #用列表存放即将提取的书据
                with open(x,'r') as summary:                            #with open() as 形式优于f=open()
                    for line in summary:                                #优于t=readlines()形式
                        if re.search('var numCountPE = {"GapR1"',line): #在re.search中特殊字符不需转义，而在re.compile()中需转义（例如\'）
                            csv_lis.append(m);csv_lis.append(sli);csv_lis.append(lan)
                            fov_nu=0;re1=0;re2=0
                            with open(y,'r') as heatmap:
                                for line in heatmap:
                                    if re.search('var fovData',line):
                                        fov_nu=line.count('accGRR')
                            with open(x,'r') as summary:
                                for line in summary:
                                    if re.search('var fqTable =',line):                                             ####熟记相关正则匹配方式
                                        read1=re.compile('read\s1\'\,\s(.*)\]\,\s\[\'read\s2').findall(line)
                                        read2=re.compile('read\s2\'\,\s(.*)\]\,\s\[\'read\stotal').findall(line)
                                        re1=int(read1[0].split(',')[2].lstrip().lstrip('\'').rstrip('\''))/int(read1[0].split(',')[1].lstrip().lstrip('\'').rstrip('\''))
                                        re2=int(read2[0].split(',')[2].lstrip().lstrip('\'').rstrip('\''))/int(read2[0].split(',')[1].lstrip().lstrip('\'').rstrip('\''))
                            with open(x,'r') as summary:
                                for line in summary:
                                    if re.search('var reportTime',line):
                                        csv_lis.append(line.split('"')[1])
                                    if re.search('var summaryTable',line):
                                        totalreads=re.compile('TotalReads\(M\)\'\,\s(.*)\]\,\s\[\'MappedReads').findall(line)[0]
                                        cyclenum=0
                                        cyc1=re.compile('CycleNumber\'\,\s([0-9]{2,3})\]\,\s\[\'').findall(line)  #关注([0-9]{2,3})
                                        if len(cyc1)==0:
                                            break
                                        else:
                                            cyclenum=int(cyc1[0])
                                        lag1=re.compile('Lag1\%\'\,\s(.*)\]\,\s\[\'Lag2').findall(line)[0]
                                        lag2=re.compile('Lag2\%\'\,\s(.*)\]\,\s\[\'Runon1').findall(line)[0]
                                        GRR=re.compile('GRR\%\'\,\s(.*)\]\,\s\[\'MappingRate').findall(line)[0]
                                        csv_lis.append(totalreads)
                                        csv_lis.append(fov_nu)
                                        csv_lis.append(cyclenum)
                                        csv_lis.append(re1)
                                        csv_lis.append(re2)
                                        csv_lis.append(lag1)
                                        csv_lis.append(lag2)
                                        csv_lis.append(GRR)
                                        
                                    if re.search('var signal',line):
                                        A=re.compile('A\"\:\s\[(.*)\]\,\s\"C').findall(line)[0] 
                                        A=A.split(',')      
                                        C=re.compile('C\"\:\s\[(.*)\]\,\s\"T').findall(line)[0]
                                        C=C.split(',')
                                        T=re.compile('T\"\:\s\[(.*)\]\,\s\"G').findall(line)[0]
                                        T=T.split(',')
                                        G=re.compile('G\"\:\s\[(.*)\]\}').findall(line)[0]
                                        G=G.split(',')
                                        csv_lis.append(A[0]);csv_lis.append(C[0]);csv_lis.append(T[0]);csv_lis.append(G[0])
                                        csv_lis.append(A[re1-1]);csv_lis.append(C[re1-1]);csv_lis.append(T[re1-1]);csv_lis.append(G[re1-1])
                                        csv_lis.append(A[re1]);csv_lis.append(C[re1]);csv_lis.append(T[re1]);csv_lis.append(G[re1])
                                        csv_lis.append(A[re1+re2-1]);csv_lis.append(C[re1+re2-1]);csv_lis.append(T[re1+re2-1]);csv_lis.append(G[re1+re2-1])
                                        for i in range(len(csv_lis)-1):
                                            print >>fo,csv_lis[i],',',
                                        print >>fo,csv_lis[-1]
fo.close()
print time.time()-t1
print 'down'
    

