from __future__ import division
import sys,os,time
###########################################################################################
################### #####统计各组合在各cycle的Q30值（相对于自身）################################
###########################################################################################
start = time.time()
fqlist = []
f = open(r'Z:\LM\G_increase\8199\8199_tst_SE120\8199_c007r022\L01\C007R022_NOPF_L01_read.fq')
t = f.readlines()
for i in range(1,len(t),4):
    fqlist.append(t[i])
t=[]
f.close()   

Q30_list = [[] for i in range(17)]
ratio = [[] for i in range(17)]
name_list = ['AA','AC','AG','AT','CA','CC','CG','CT','GA','GC','GG','GT','TA','TC','TG','TT','total']
def Q30_stat(cycle):
    #su = 0
    Q30 = 0  #记录总体中score>=30的reads数
    mer = 0  #记录总体中不含N的2base个数
    int_list2 = [] 
    dic=dict([[0,'A'],[1,'C'],[2,'G'],[3,'T']])
    f2 = open(r'Z:\LM\G_increase\8199\8199_tst_SE120\8199_c007r022\L01\Intensities\finInts\%s\C007R022.txt'%cycle[0])
    t2 = f2.readlines()
    for i in range(len(t2)):
        t2[i] = t2[i].split()
        fourint = []
        for j in range(4):
            fourint.append(float(t2[i][j]))
        int_list2.append(fourint)

    type_list = [['AA',0],['AC',0],['AG',0],['AT',0],['CA',0],['CC',0],['CG',0],['CT',0],\
             ['GA',0],['GC',0],['GG',0],['GT',0],['TA',0],['TC',0],['TG',0],['TT',0],['total',0]]
    mer_dict = dict(type_list)
    Q30_num = dict(type_list)
    for i in range(len(t2)):
        x = fqlist[i][(cycle[1]-2):cycle[1]]   #从fq截取2bp，开始计算
        if x in mer_dict:
            mer += 1
            mer_dict[x] += 1
            s_list = int_list2[i]
            if max(s_list) > 0:
                for j in range(4):
                    if s_list[j] < 0:
                        s_list[j] = 0
                score = int(40*(max(s_list)/sum(s_list)-0.25)/0.75)
                if score >= 30:
                    Q30_num[x] += 1
                    Q30 += 1

    for i in range(16):
        Q30_list[i].append('%.2f'%((Q30_num[name_list[i]]/mer_dict[name_list[i]])*100))
        ratio[i].append('%.2f'%((mer_dict[name_list[i]]/len(t2))*100))
    Q30_list[16].append('%.2f'%((Q30/len(t2))*100))  #总体Q30
    ratio[16].append('%.2f'%((mer/len(t2))*100))
cycle = []
cycle_list = ['S00%d'%i for i in range(2,10)]
cycle_list.extend(['S0%d'%i for i in range(10,100)])
cycle_list.extend(['S%d'%i for i in range(100,121)])
for i in range(2,121):
    cycle.append([cycle_list[i-2],i])

for cycle in cycle:
    Q30_stat(cycle)
fo = open('wholeCycleQ30stat.csv','w+')
fo.write('cycle,')
for i in range(2,121):
    fo.write('%d,'%i)
fo.write('\n')
for i in range(17):
    temp_list = [name_list[i]]
    temp_list.extend(Q30_list[i])
    fo.write(','.join(temp_list))
    fo.write('\n')
fo.write('\n\n\n')
fo.write('cycle,')
for i in range(2,121):
    fo.write('%d,'%i)
fo.write('\n')
for i in range(17):
    temp_list = [name_list[i]]
    temp_list.extend(ratio[i])
    fo.write(','.join(temp_list))
    fo.write('\n')
fo.close()
print time.time()-start


