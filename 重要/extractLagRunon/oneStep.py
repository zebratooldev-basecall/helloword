import sys,os
import shutil


path = sys.path[0]
dirList = ['AllFov','C005R008','C005R032','C006R013','C012R008','C012R032']
def cpFile(srcPath,destDir):
    fileName = os.path.basename(srcPath)
    destPath = destDir + os.path.sep + fileName
    if os.path.exists(srcPath) and not os.path.exists(destPath):
        shutil.copy(srcPath,destPath)
    return destPath

srcPath = path + os.path.sep + 'getLagRunOnCsvData.py'
for dirName in dirList:
    destDir = path + os.path.sep + dirName
    destPath = cpFile(srcPath,destDir)
    os.chdir(destDir)
    print os.getcwd()
    os.system('python %s'%(destPath))
    os.remove(destPath)
    os.chdir(path)
    print os.getcwd()
print 'done'

