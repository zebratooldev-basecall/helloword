import os,sys
import tarfile,glob
import shutil
slide='GS90001966-FS3'  
l_cyc=24
r_cyc=27
lag=[[],[],[],[],[],[],[],[]]
runon=[[],[],[],[],[],[],[],[]]
###### 
path=sys.path[0]                
def untar(fname, path):         
    t = tarfile.open(fname)
    t.extractall(path = path)
tar_list = glob.glob(os.path.join(path, '*tar.gz'))
for i in tar_list:
    untar(i, path)

########################################################################################################
#####################################  ##########################################
########################################################################################################
for i in range(2,10):
    f=open(r'%s\%s-C1-C10\%s.output.C%d.csv'%(path,slide,slide,i),'rb')
    t=f.readlines()
    for j in range(9,17):
        lag[j-9].append(t[j].split(',')[2])
    f.close()

for i in range(12,20):
    f=open(r'%s\%s-C11-C20\%s.output.C%d.csv'%(path,slide,slide,i),'rb')
    t=f.readlines()
    for j in range(9,17):
        lag[j-9].append(t[j].split(',')[2])
    f.close()

for i in range(22,30):
    f=open(r'%s\%s-C21-C30\%s.output.C%d.csv'%(path,slide,slide,i),'rb')
    t=f.readlines()
    for j in range(9,17):
        lag[j-9].append(t[j].split(',')[2])
    f.close()


########################################################################################################
##################################### ########################################
########################################################################################################
for i in range(1,10):
    f=open(r'%s\%s-C1-C10\%s.output.C%d.csv'%(path,slide,slide,i),'rb')
    t=f.readlines()
    for j in range(9,17):
        runon[j-9].append(t[j].split(',')[3].strip('\n'))
    f.close()

for i in range(11,20):
    f=open(r'%s\%s-C11-C20\%s.output.C%d.csv'%(path,slide,slide,i),'rb')
    t=f.readlines()
    for j in range(9,17):
        runon[j-9].append(t[j].split(',')[3].strip('\n'))
    f.close()
 
for i in range(21,30):
    f=open(r'%s\%s-C21-C30\%s.output.C%d.csv'%(path,slide,slide,i),'rb')
    t=f.readlines()
    for j in range(9,17):
        runon[j-9].append(t[j].split(',')[3].strip('\n'))
    f.close()

##########################################################################################
fo=open('%s-lagRunOn.csv'%slide,'a+')
print >>fo,'lag',',',
for i in range(1,l_cyc):
    print >>fo,'C%d'%i,',', 
print >>fo,'C%d'%l_cyc

for i in range(1,9):
    print >>fo,'L0%d'%i,',',
    for j in range(l_cyc-1):
        print >>fo,lag[i-1][j],',',
    print >>fo,lag[i-1][l_cyc-1]

print >>fo,'\n','\n','\n'

print >>fo,'RunOn',',',
for i in range(1,r_cyc):
    print >>fo,'C%d'%i,',',
print >>fo,'C%d'%r_cyc
for i in range(1,9):
    print >>fo,'L0%d'%i,',',
    for j in range(r_cyc-1):
        print >>fo,runon[i-1][j],',',
    print >>fo,runon[i-1][r_cyc-1]
fo.close()

#####
dirList = glob.glob(os.path.join(path, 'GS*'))
dirList = [d for d in dirList if os.path.isdir(d)]
for i in dirList:
    shutil.rmtree(i)
print 'done'
