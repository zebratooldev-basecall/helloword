from __future__ import division
import sys,os
import copy,time
time1=time.time()
cyc_num='110'            #####cycle input######
######################## separate stst file ###############################
f=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\C007R022_cycle%s_stat.txt'%cyc_num,'rb')
t=f.readlines()
t1=copy.deepcopy(t)
for i in range(len(t)):
    t[i]=t[i].split()
#f1=open('unmapped.txt','a+')
f2=open('cycle%scall_right.txt'%cyc_num,'a+')
f3=open('cycle%scall_false.txt'%cyc_num,'a+')
for i in range(len(t)):
    if t[i][1]==t[i][2] and t[i][1]!='N':
        print >>f2,t1[i].strip('\n').strip('\r')
    elif t[i][1]!=t[i][2] and t[i][2]!='N':
        print >>f3,t1[i].strip('\n').strip('\r')
    #else:
       # print >>f1,t1[i].strip('\n').strip('\r')
#f1.close()
f2.close()
f3.close()
t1=t=[]
time.sleep(3)

##############################################################
###################calculate mismatch type####################
##############################################################
f=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%scall_false.txt'%cyc_num,'rb')
t=f.readlines()
t1=copy.deepcopy(t)
for i in range(len(t)):
    t[i]=t[i].split()

mid_fin=0
raw_fin=0
equal=0
notequ=0
for i in range(len(t)):
    if t[i][1]!=t[i][7] and t[i][1]==t[i][5]:
        mid_fin+=1
    elif t[i][1]!=t[i][5] and t[i][1]==t[i][7]:
        raw_fin+=1
    elif t[i][1]==t[i][7] and t[i][1]==t[i][5]:
        equal+=1
    elif t[i][1]!=t[i][7] and t[i][1]!=t[i][5]:
        notequ+=1
f.close()
print mid_fin
print raw_fin
print equal    
print notequ
#################calculate mismatch type###############

A_G=0;C_G=0;T_G=0;C_A=0;G_A=0;T_A=0
A_C=0;G_C=0;T_C=0;A_T=0;C_T=0;G_T=0
for i in range(len(t)):
    if t[i][2]=='A' and t[i][1]=='C':
        A_C+=1
    elif t[i][2]=='A' and t[i][1]=='G':
        A_G+=1
    elif t[i][2]=='A' and t[i][1]=='T':
        A_T+=1
    elif t[i][2]=='C' and t[i][1]=='A':
        C_A+=1
    elif t[i][2]=='C' and t[i][1]=='G':
        C_G+=1
    elif t[i][2]=='C' and t[i][1]=='T':
        C_T+=1
    elif t[i][2]=='G' and t[i][1]=='A':
        G_A+=1
    elif t[i][2]=='G' and t[i][1]=='C':
        G_C+=1
    elif t[i][2]=='G' and t[i][1]=='T':
        G_T+=1
    elif t[i][2]=='T' and t[i][1]=='A':
        T_A+=1
    elif t[i][2]=='T' and t[i][1]=='C':
        T_C+=1
    elif t[i][2]=='T' and t[i][1]=='G':
        T_G+=1

################## 起始出错分布#####################################
gr=0;gm=0;gf=0;gx=0;tr=0;tm=0;tf=0;tx=0;ar=0;am=0;af=0;ax=0;cr=0;cm=0;cf=0;cx=0

fgr=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%s\Gmisat_r.txt'%cyc_num,'a+')
fgm=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%s\Gmisat_m.txt'%cyc_num,'a+')
fgf=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%s\Gmisat_f.txt'%cyc_num,'a+')
fgx=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%s\Gmisat_x.txt'%cyc_num,'a+')

fcr=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%s\Cmisat_r.txt'%cyc_num,'a+')
fcm=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%s\Cmisat_m.txt'%cyc_num,'a+')
fcf=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%s\Cmisat_f.txt'%cyc_num,'a+')
fcx=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%s\Cmisat_x.txt'%cyc_num,'a+')

ftr=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%s\Tmisat_r.txt'%cyc_num,'a+')
ftm=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%s\Tmisat_m.txt'%cyc_num,'a+')
ftf=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%s\Tmisat_f.txt'%cyc_num,'a+')
ftx=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%s\Tmisat_x.txt'%cyc_num,'a+')

far=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%s\Amisat_r.txt'%cyc_num,'a+')
fam=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%s\Amisat_m.txt'%cyc_num,'a+')
faf=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%s\Amisat_f.txt'%cyc_num,'a+')
fax=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%s\Amisat_x.txt'%cyc_num,'a+')
for i in range(len(t)):
    if t[i][1]=='G':        
        if t[i][5]!=t[i][2] and t[i][5]==t[i][7]==t[i][1]:
            gr+=1
            print >>fgr,t1[i].strip('\n').strip('\r')
        elif t[i][5]==t[i][2] and t[i][7]!=t[i][2] and t[i][7]==t[i][1]:
            gm+=1
            print >>fgm,t1[i].strip('\n').strip('\r')
        elif t[i][5]==t[i][2] and t[i][7]==t[i][2]:
            gf+=1
            print >>fgf,t1[i].strip('\n').strip('\r')
        else:
            gx+=1
            print >>fgx,t1[i].strip('\n').strip('\r')
          
    elif t[i][1]=='T':
        if t[i][5]!=t[i][2] and t[i][5]==t[i][7]==t[i][1]:
            tr+=1
            print >>ftr,t1[i].strip('\n').strip('\r')
        elif t[i][5]==t[i][2] and t[i][7]!=t[i][2] and t[i][7]==t[i][1]:
            tm+=1
            print >>ftm,t1[i].strip('\n').strip('\r')
        elif t[i][5]==t[i][2] and t[i][7]==t[i][2]:
            tf+=1
            print >>ftf,t1[i].strip('\n').strip('\r')
        else:
            tx+=1
            print >>ftx,t1[i].strip('\n').strip('\r')
            
    elif t[i][1]=='A':
        if t[i][5]!=t[i][2] and t[i][5]==t[i][7]==t[i][1]:
            ar+=1
            print >>far,t1[i].strip('\n').strip('\r')
        elif t[i][5]==t[i][2] and t[i][7]!=t[i][2] and t[i][7]==t[i][1]:
            am+=1
            print >>fam,t1[i].strip('\n').strip('\r')
        elif t[i][5]==t[i][2] and t[i][7]==t[i][2]:
            af+=1
            print >>faf,t1[i].strip('\n').strip('\r')
        else:
            ax+=1
            print >>fax,t1[i].strip('\n').strip('\r')
            
    elif t[i][1]=='C':
        if t[i][5]!=t[i][2] and t[i][5]==t[i][7]==t[i][1]:
            cr+=1
            print >>fcr,t1[i].strip('\n').strip('\r')
        elif t[i][5]==t[i][2] and t[i][7]!=t[i][2] and t[i][7]==t[i][1]:
            cm+=1
            print >>fcm,t1[i].strip('\n').strip('\r')
        elif t[i][5]==t[i][2] and t[i][7]==t[i][2]:
            cf+=1
            print >>fcf,t1[i].strip('\n').strip('\r')
        else:
            cx+=1
            print >>fcx,t1[i].strip('\n').strip('\r')
fgr.close();fgm.close();fgf.close();fgx.close();far.close();fam.close();faf.close();fax.close();
fcr.close();fcm.close();fcf.close();fcx.close();ftr.close();ftm.close();ftf.close();ftx.close();

fo=open(r'Z:\LM\G_increase\SE120\xudongyang\cycle114\cycle%s\cycle%smismatch.txt'%(cyc_num,cyc_num),'a+')
su=len(t)
print >>fo,'A->G'.ljust(5),'C->G'.ljust(5),'T->G'.ljust(5),'M->G'.ljust(5),'misatr','misatm','misatf','misatx'
print >>fo,'%.2f'%((A_G/su)*100),'%.2f'%((C_G/su)*100),'%.2f'%((T_G/su)*100),float('%.2f'%((A_G/su)*100))+float('%.2f'%((C_G/su)*100))+float('%.2f'%((T_G/su)*100)),\
      '%.2f'%((gr/su)*100),'%.2f'%((gm/su)*100),'%.2f'%((gf/su)*100),'%.2f'%((gx/su)*100),'\n'
print >>fo,'A->C','G->C','T->C','M->C','misatr','misatm','misatf'
print >>fo,'%.2f'%((A_C/su)*100),'%.2f'%((G_C/su)*100),'%.2f'%((T_C/su)*100),float('%.2f'%((A_C/su)*100))+float('%.2f'%((G_C/su)*100))+float('%.2f'%((T_C/su)*100)),\
      '%.2f'%((cr/su)*100),'%.2f'%((cm/su)*100),'%.2f'%((cf/su)*100),'%.2f'%((cx/su)*100),'\n'
print >>fo,'A->T','C->T','G->T','M->T','misatr','misatm','misatf'
print >>fo,'%.2f'%((A_T/su)*100),'%.2f'%((C_T/su)*100),'%.2f'%((G_T/su)*100),float('%.2f'%((A_T/su)*100))+float('%.2f'%((C_T/su)*100))+float('%.2f'%((G_T/su)*100)),\
      '%.2f'%((tr/su)*100),'%.2f'%((tm/su)*100),'%.2f'%((tf/su)*100),'%.2f'%((tx/su)*100),'\n'
print >>fo,'C->A','G->A','T->A','M->A','misatr','misatm','misatf'
print >>fo,'%.2f'%((C_A/su)*100),'%.2f'%((G_A/su)*100),'%.2f'%((T_A/su)*100),float('%.2f'%((C_A/su)*100))+float('%.2f'%((G_A/su)*100))+float('%.2f'%((T_A/su)*100)),\
      '%.2f'%((ar/su)*100),'%.2f'%((am/su)*100),'%.2f'%((af/su)*100),'%.2f'%((ax/su)*100),'\n'

print >>fo,'midcall differ fincall:',mid_fin,'\t','%.3f'%((mid_fin/su)*100)
print >>fo,'rawcall differ fincall:',raw_fin,'\t','%.3f'%((raw_fin/su)*100)
print >>fo,'raw mid fin all equal:',equal,'\t','%.3f'%((equal/su)*100)
print >>fo,'fin notequall raw mid:',notequ,'\t','%.3f'%((notequ/su)*100)

fo.close()
f.close()
print time.time()-time1
