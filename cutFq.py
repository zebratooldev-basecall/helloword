import sys
import os
import gzip

if len(sys.argv) != 4:
    print 'EXE: <fq.gz> <top/end> <finalLength> '
    sys.exit()

def cutFq(fq, pos, length):
    if not os.path.exists(fq):
        return

    fqAddr = os.path.dirname(fq)
    newfq = os.path.join(fqAddr, 'temp.fq.gz')
    fhOut = gzip.open(newfq, 'w')

    if pos == 'top':
        pass
    elif pos == 'end':
        pass
    else:
        print 'Wrong Parameter.\n'
        return

    with gzip.open(fq, 'r') as fhIn:
        idLine = 1
        while idLine:
            idLine = fhIn.readline().strip()
            seqLine = fhIn.readline().strip()
            plusLine = fhIn.readline().strip()
            qualityLine = fhIn.readline().strip()

            if not idLine:
                break

            string = ''
            string += idLine+'\n'
            if pos == 'top':
                string += seqLine[:length]+'\n'
            else:
                string += seqLine[-length:]+'\n'

            string += plusLine+'\n'

            if pos == 'top':
                string += qualityLine[:length]+'\n'
            else:
                string += qualityLine[-length:]+'\n'

            fhOut.write(string)

    fhIn.close()
    fhOut.close()

    
cutFq(sys.argv[1], sys.argv[2], int(sys.argv[3]))


