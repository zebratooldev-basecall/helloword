#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
import re
import glob
import gzip
# import json
# import pprint
# import cPickle as pickle

###### Document Decription
'''  '''

###### Version and Date
prog_version = '1.1.0'
prog_date = '2015-04-02'

###### Usage
usage = '''

     Version %s  by Vincent-Li  %s

     Usage: %s <barcodeList> <fastqFile> <outDir> 
''' % (prog_version, prog_date, os.path.basename(sys.argv[0])) #sys.argv[]是用来获取命令行参数的，sys.argv[0]表示（本）代码本身文件路径

######## Global Variable


#######################################################################
############################  BEGIN Class  ############################
#######################################################################


##########################################################################
############################  BEGIN Function  ############################
##########################################################################
def createBarcodeDict(fileName, barh,ofhDict , outd, err=1): #outd是路径喽？
    barSet = set()  #无序不重复元素集                   #ofhDict是
    ########### Open and read input file
    try:
        fhIn = open(fileName, 'r') #以读模式打开文件
    except Exception as e:         #当有错时引发异常（无错则继续）
        raise e
  ##其实读取的大文件是由许多个序列的fastq文件合并起来的，所以以下的过程就是找出以数字开头
  #的那一行（该数字应该就是对应的barcod的编号）
    
    for line in fhIn:              #逐行读取文件
        info = line.split()        #分隔（切片），没参数则默认按空格来切片
        if len(info) < 2:
            continue
        if re.match(r'^\d+$', info[0]):  #从开头开始匹配“^\d+$”表示整数，即数字开头，
            info[0] = "barcode_" + info[0]
        createBarHash(info[0], info[1], barh, err) #我去！这是后面定义的一个函数

        barSet.add(info[1])    #barset收集“匹配行”的第二部分（实则是序列）

        ########### Open and write file
        try:
            newFh = gzip.open(os.path.join(outd, "%s.fq.gz" % info[0]), 'w') #创建压缩文件
        except Exception, e:                      #%s 是字符串格式化
            raise e
        ofhDict[info[0]] = newFh;       #将压缩文件与编号建立键值对应

        ##### ambiguous.fq
        if "ambiguous" in barh.values(): #检查字串是否为字典barh中的值（而非键）
            try:
                ambiFh = gzip.open(os.path.join(outd, "%s.fq.gz" % "ambiguous"), 'w')
            except Exception, e:
                raise e
            ofhDict["ambiguous"] = ambiFh;

    fhIn.close
    return barSet #返回的是收集到的“序列”

def createBarHash(name, seq, bh, err=1): #bh是字典
    if err == 0:
        bh[seq] = name
        return bh
    baseList = ('A', 'C', 'G', 'T', 'N')  #这是当有错配时的处理
    for idx in xrange(0, len(seq)):    #seq是序列
        for b in baseList:
            tmpSeq = seq[:idx] + b + seq[idx+1:]
            if err > 1:
                createBarHash(name, tmpSeq, bh, err-1)  #递归
           
            if tmpSeq in bh and bh[tmpSeq] != name:
                bh[tmpSeq] = 'ambiguous'
            else:
                bh[tmpSeq] = name
    return bh

def splitBarcode(fqFile, barh, fhd, fc=36, lc=45):
    statDict = {}
    ########### Open and read input file
    try:
        if fqFile.endswith('.gz'):
            fqIn = gzip.open(fqFile, 'r', 100000000)
        else:
            fqIn = open(fqFile, 'r', 100000000)
    except Exception as e:
        raise e
    
    idLine = "1"
    while idLine:
        idLine = fqIn.readline()
        seqLine = fqIn.readline()
        plusLine = fqIn.readline()
        qualLine = fqIn.readline()
        barcodeSeq = seqLine[fc-1:lc]

        if barcodeSeq not in statDict:
            statDict[barcodeSeq] = 0
        statDict[barcodeSeq] += 1

        if barcodeSeq in barh:
            ofh = fhd[barh[barcodeSeq]]
            ofh.write(idLine)
            ofh.write(seqLine[:fc-1] + seqLine[lc:])
            ofh.write(plusLine)
            ofh.write(qualLine[:fc-1] + qualLine[lc:])

    fqIn.close
    return statDict

def outStat(statd, barh, bset, outDir):
    total = sum(statd.values())
    klist = sorted(statd.keys(), key=lambda x: statd[x], reverse=True)
    statDict = {}
    ########### Open and write file
    try:
        tagStat = open(os.path.join(outDir, "tagStatistics.txt"), 'w')
        splitStat = open(os.path.join(outDir, "splitRate.txt"), 'w')
    except Exception, e:
        raise e
    
    for k in klist:
        bar = "unknown"
        if k in barh:
            bar = barh[k]
            if bar not in statDict:
                statDict[bar] = {"correct": 0, "corrected": 0}
            if k in bset:
                statDict[bar]['correct'] += statd[k]
            else:
                statDict[bar]['corrected'] += statd[k]
        tagStat.write("%s\t%s\t%s\t%.3f\n" % (k ,bar, statd[k], 100.0*statd[k]/total))
    tagStat.close
    splitStat.write("#barcode\tCorrect\tCorrected\tTotal\tPct%\n")
    allCorrect = 0
    allCorrected = 0
    for b in sorted(statDict.keys()):
        splitedNum = statDict[b]['correct'] + statDict[b]['corrected']
        splitStat.write("%s\t%s\t%s\t%s\t%.3f\n" % (b, statDict[b]['correct'], statDict[b]['corrected'], splitedNum, 100.0*splitedNum/total))
        allCorrect += statDict[b]['correct']
        allCorrected += statDict[b]['corrected']
    splitStat.write("Total\t%s\t%s\t%s\t%.3f\n" % (allCorrect, allCorrected, allCorrect + allCorrected, 100.0*(allCorrect + allCorrected)/total))

######################################################################
############################  BEGIN Main  ############################
######################################################################
#################################
##
##   Main function of program.
##
#################################
def main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)
    ArgParser.add_argument("-e", "--errNum", action="store", dest="errNum", type=int, default=2, metavar="INT", help=" Allow mismatch count in barcode sequence. [%(default)s]")
    ArgParser.add_argument("-f", "--firstCycle", action="store", dest="firstCycle", type=int, default=36, metavar="INT", help="First cylce of barcode. [%(default)s]")
    ArgParser.add_argument("-l", "--lastCycle", action="store", dest="lastCycle", type=int, default=45, metavar="INT", help="Last cycle of barcode. [%(default)s]")

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 3:
        ArgParser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (barcodeList, fastqFile, outDir) = args

    ############################# Main Body #############################
    barcodeHash = {}
    outFh = {}
    barcodeSet = createBarcodeDict(barcodeList, barcodeHash, outFh, outDir, para.errNum)
    splitStat = splitBarcode(fastqFile, barcodeHash, outFh, para.firstCycle, para.lastCycle)
    outStat(splitStat, barcodeHash, barcodeSet, outDir)


#################################
##

##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()

################## God's in his heaven, All's right with the world. ##################
