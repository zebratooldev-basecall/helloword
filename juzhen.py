import numpy as np
a=np.array([1,3,2,4,5,67,5,4,88]).reshape(3,3)
b=np.array([23,86,55]).reshape(-1,1)
print a
print b
c=a*b
print c
print b*a
d=np.array([1,6,234,67])
e=np.array([97,345,9,3]).reshape(-1,1)
print np.dot(d,e)
#print np.dot(e,d)
f=np.array([1,2,3,4,5,6,6,7,9,64,2,5,23,45,8,9]).reshape(4,4)
g=np.array([12,3,5,11]).reshape(-1,1)
print f
print g
print f*g
print np.dot(f,g)
print '\n'
h=np.array([1,2,3,4]).reshape(2,2)
i=np.array([5,6,7,8]).reshape(2,2)
print np.dot(h,i)
print np.linalg.norm(np.dot(f,g))
print np.linalg.norm(np.dot(f,g),2)
print np.linalg.norm(f*g)
print np.linalg.norm(f*g,2)

