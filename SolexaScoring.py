# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 10:40:06 2015

@author: xudongyang
"""
from __future__ import division 
import numpy as np
import math
#from scipy import stats as st
import time
t1=time.time()
f=open(r'C:\Users\xudongyang\Desktop\scoreing\test.txt')
test=f.readlines()
tt1=time.time()
a=[(float((i.split())[0])) for i in test]
c=[(float((i.split())[1])) for i in test]
g=[(float((i.split())[2])) for i in test]
t=[(float((i.split())[3])) for i in test]
s=[a,c,g,t]
tt2=time.time()
print tt2-tt1
N=[]
an=[]     
cn=[]
gn=[]
tn=[]
for i in range(len(test)):
    if i<10:                
        down=0
    elif i>=len(test)-10:
        down=len(test)-20
    else:
        down=i-10
    up=down+20
    
    an.append(min(s[0][down:up]))
    cn.append(min(s[1][down:up]))
    gn.append(min(s[2][down:up]))
    tn.append(min(s[3][down:up]))

N=[an,cn,gn,tn]
cov=np.cov(N)            
print cov
print '\n'
chol=np.linalg.cholesky(cov) 
print chol
print '\n'
tras=np.transpose(chol)     
print tras

Q1=0
Q2=0
Q3=0
Q4=0
#W=[]
for i in range(0,len(test)):
    u=np.array([s[0][i],s[1][i],s[2][i],s[3][i]]).reshape((-1,1))
    pvec=0
    dvecA=[0,0,0]
    n1=np.array([1/math.sqrt(2),-1/math.sqrt(2),0,0]).reshape((-1,1))
    n2=np.array([1/math.sqrt(2),0,-1/math.sqrt(2),0]).reshape((-1,1))
    n3=np.array([1/math.sqrt(2),0,0,-1/math.sqrt(2)]).reshape((-1,1))
    dvecA[0]=abs(np.dot(u.reshape(1,-1),n1))/np.linalg.norm(np.dot(tras,n1),2) 
    dvecA[1]=abs(np.dot(u.reshape(1,-1),n2))/np.linalg.norm(np.dot(tras,n2),2)
    dvecA[2]=abs(np.dot(u.reshape(1,-1),n3))/np.linalg.norm(np.dot(tras,n3),2)
    
    dvecC=[0,0,0]
    n1=np.array([-1/math.sqrt(2),1/math.sqrt(2),0,0]).reshape((-1,1))
    n2=np.array([0,1/math.sqrt(2),-1/math.sqrt(2),0]).reshape((-1,1))
    n3=np.array([0,1/math.sqrt(2),0,-1/math.sqrt(2)]).reshape((-1,1))
    dvecC[0]=abs(np.dot(u.reshape(1,-1),n1))/np.linalg.norm(np.dot(tras,n1),2)
    dvecC[1]=abs(np.dot(u.reshape(1,-1),n2))/np.linalg.norm(np.dot(tras,n2),2)
    dvecC[2]=abs(np.dot(u.reshape(1,-1),n3))/np.linalg.norm(np.dot(tras,n3),2)
    
    dvecG=[0,0,0]
    n1=np.array([-1/math.sqrt(2),0,1/math.sqrt(2),0]).reshape((-1,1))
    n2=np.array([0,-1/math.sqrt(2),1/math.sqrt(2),0]).reshape((-1,1))
    n3=np.array([0,0,1/math.sqrt(2),-1/math.sqrt(2)]).reshape((-1,1))
    dvecG[0]=abs(np.dot(u.reshape(1,-1),n1))/np.linalg.norm(np.dot(tras,n1),2)
    dvecG[1]=abs(np.dot(u.reshape(1,-1),n2))/np.linalg.norm(np.dot(tras,n2),2)
    dvecG[2]=abs(np.dot(u.reshape(1,-1),n3))/np.linalg.norm(np.dot(tras,n3),2)
    
    dvecT=[0,0,0]
    n1=np.array([-1/math.sqrt(2),0,0,1/math.sqrt(2)]).reshape((-1,1))
    n2=np.array([0,-1/math.sqrt(2),0,1/math.sqrt(2)]).reshape((-1,1))
    n3=np.array([0,0,-1/math.sqrt(2),1/math.sqrt(2)]).reshape((-1,1))
    dvecT[0]=abs(np.dot(u.reshape(1,-1),n1))/np.linalg.norm(np.dot(tras,n1),2)
    dvecT[1]=abs(np.dot(u.reshape(1,-1),n2))/np.linalg.norm(np.dot(tras,n2),2)
    dvecT[2]=abs(np.dot(u.reshape(1,-1),n3))/np.linalg.norm(np.dot(tras,n3),2)
    
    if max(u)==u[0]:
        pvecc=0.5-0.5*(math.erf(np.linalg.norm(dvecC)/math.sqrt(2.5)))
        pvecg=0.5-0.5*(math.erf(np.linalg.norm(dvecG)/math.sqrt(2.5)))
        pvect=0.5-0.5*(math.erf(np.linalg.norm(dvecT)/math.sqrt(2.5)))
        pvec=1-pvecc-pvecg-pvect
    elif max(u)==u[1]:
        pveca=0.5-0.5*(math.erf(np.linalg.norm(dvecA)/math.sqrt(2.5)))
        pvecg=0.5-0.5*(math.erf(np.linalg.norm(dvecG)/math.sqrt(2.5)))
        pvect=0.5-0.5*(math.erf(np.linalg.norm(dvecT)/math.sqrt(2.5)))
        pvec=1-pveca-pvecg-pvect
    elif max(u)==u[2]:
        pveca=0.5-0.5*(math.erf(np.linalg.norm(dvecA)/math.sqrt(2.5)))
        pvecc=0.5-0.5*(math.erf(np.linalg.norm(dvecC)/math.sqrt(2.5)))
        pvect=0.5-0.5*(math.erf(np.linalg.norm(dvecT)/math.sqrt(2.5)))
        pvec=1-pveca-pvecc-pvect
    else:
        pveca=0.5-0.5*(math.erf(np.linalg.norm(dvecA)/math.sqrt(2.5)))
        pvecc=0.5-0.5*(math.erf(np.linalg.norm(dvecC)/math.sqrt(2.5)))
        pvecg=0.5-0.5*(math.erf(np.linalg.norm(dvecG)/math.sqrt(2.5)))
        pvec=1-pveca-pvecc-pvecg
        
    x=pvec
    q=0
    if x<0.000001:
        q = -40
    elif x > 0.999999:
        q=40
    else:
        q=10*math.log10(x/(1-x))
    w=q
    #W.append(w)
    if w>=10:
        Q1+=1
    if w>=20:
        Q2+=1
    if w>=30:
        Q3+=1
    if w>=40:
        Q4+=1
Q10=Q1/len(test)
Q20=Q2/len(test)
Q30=Q3/len(test)
Q40=Q4/len(test)

print 'The value of Q10 is: %.3f'%(Q10*100)
print 'The value of Q20 is: %.3f'%(Q20*100)
print 'The value of Q30 is: %.3f'%(Q30*100)
print 'The value of Q40 is: %.3f'%(Q40*100)

t2=time.time()
print '\n'
print t2-t1

#print tras*np.array([-1/math.sqrt(2),0,0,1/math.sqrt(2)]).reshape((-1,1))

