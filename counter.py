#!/usr/bin/env python
#coding: utf8
from __future__ import division
import os
import sys
#from __future__ import division
###########################也是统计碱基分布#####################################################
A=[0 for i in range(5)]
T=A[:] #不直接=A，
C=A[:]
G=A[:]
f=open('/prod/pv-01/research/st_read/F13ZOOYJSY1380/limei1/xudongyang/cr0605/L02/barcode_8.fq')
lis=f.readlines()
tarnu=range(2,len(lis),4)
WCW=[]
for i in tarnu:
    WCW.append(lis[i-1])
for i in WCW:
    for j in range(5):
        if i[j]=='A':
            A[j]+=1
        elif i[j]=='T':
            T[j]+=1
        elif i[j]=='C':
            C[j]+=1
        elif i[j]=='G':
            G[j]+=1
PA=[]
PT=[]
PC=[]
PG=[]
for i in range(5):
    PA.append(float('%.6f'%(A[i]/len(WCW))))  #可以简化for
    PT.append(float('%.6f'%(T[i]/len(WCW))))
    PC.append(float('%.6f'%(C[i]/len(WCW))))
    PG.append(float('%.6f'%(G[i]/len(WCW))))
fo=open('CR0605_L02_statis.txt','a+')
print >>fo,'CR0605_L02_barcode_8.fq'
print >>fo,'A',PA
print >>fo,'T',PT
print >>fo,'C',PC
print >>fo,'G',PG
print >>fo,'\n'
f.close()

