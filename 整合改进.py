import random
import numpy as np
import math

a=[]
c=[]
g=[]
t=[]
for i in range(100000):
    a.append(random.uniform(-1,1))
    c.append(random.uniform(-1,1))
    g.append(random.uniform(-1,1))
    t.append(random.uniform(-1,1))
s=[a,c,g,t]

N=[]
an=[]
cn=[]
gn=[]
tn=[]
for i in range(100000):
    if i<10:
        down=0
    elif i>=99990:
        down=99980
    else:
        down=i-10
    up=down+20
    an.append(min(s[0][down:up]))
    cn.append(min(s[1][down:up]))
    gn.append(min(s[2][down:up]))
    tn.append(min(s[3][down:up]))
N=[an,cn,gn,tn]

cov=np.cov(N)
print cov
print '\n'
chol=np.linalg.cholesky(cov) #乔里斯基分解
print chol
print '\n'
tras=np.transpose(chol)     #chol的转置
print tras

U=[]
for i in range(0,100):
    u=np.array([s[0][i],s[1][i],s[2][i],s[3][i]]).reshape((-1,1))
    U.append(u)
#u1=np.array([ai,ci,gi,ti])
#u=u1.reshape((-1,1))
#print u[1]
#print max(u)

###确定u中最大值，计算devc###
#print max(u)

def dvec(x,y):
    dve=[0,0,0]
    if max(x)==x[0]:
        n1=np.array([1/math.sqrt(2),-1/math.sqrt(2),0,0]).reshape((-1,1))
        n2=np.array([1/math.sqrt(2),0,-1/math.sqrt(2),0]).reshape((-1,1))
        n3=np.array([1/math.sqrt(2),0,0,-1/math.sqrt(2)]).reshape((-1,1))
        dve[0]=np.linalg.norm(x.reshape((1,-1))*n1)/np.linalg.norm(y*n1,2)
        dve[1]=np.linalg.norm(x.reshape((1,-1))*n2)/np.linalg.norm(y*n2,2)
        dve[2]=np.linalg.norm(x.reshape((1,-1))*n3)/np.linalg.norm(y*n3,2)
    elif max(x)==x[1]:
        n1=np.array([-1/math.sqrt(2),1/math.sqrt(2),0,0]).reshape((-1,1))
        n2=np.array([0,1/math.sqrt(2),-1/math.sqrt(2),0]).reshape((-1,1))
        n3=np.array([0,1/math.sqrt(2),0,-1/math.sqrt(2)]).reshape((-1,1))
        dve[0]=np.linalg.norm(x.reshape((1,-1))*n1)/np.linalg.norm(y*n1,2)
        dve[1]=np.linalg.norm(x.reshape((1,-1))*n2)/np.linalg.norm(y*n2,2)
        dve[2]=np.linalg.norm(x.reshape((1,-1))*n3)/np.linalg.norm(y*n3,2)
    elif max(x)==x[2]:
        n1=np.array([-1/math.sqrt(2),0,1/math.sqrt(2),0]).reshape((-1,1))
        n2=np.array([0,-1/math.sqrt(2),1/math.sqrt(2),0]).reshape((-1,1))
        n3=np.array([0,0,1/math.sqrt(2),-1/math.sqrt(2)]).reshape((-1,1))
        dve[0]=np.linalg.norm(x.reshape((1,-1))*n1)/np.linalg.norm(y*n1,2)
        dve[1]=np.linalg.norm(x.reshape((1,-1))*n2)/np.linalg.norm(y*n2,2)
        dve[2]=np.linalg.norm(x.reshape((1,-1))*n3)/np.linalg.norm(y*n3,2)
    else:
        n1=np.array([-1/math.sqrt(2),0,0,1/math.sqrt(2)]).reshape((-1,1))
        n2=np.array([0,-1/math.sqrt(2),0,1/math.sqrt(2)]).reshape((-1,1))
        n3=np.array([0,0,-1/math.sqrt(2),1/math.sqrt(2)]).reshape((-1,1))
        dve[0]=np.linalg.norm(x.reshape((1,-1))*n1)/np.linalg.norm(y*n1,2)
        dve[1]=np.linalg.norm(x.reshape((1,-1))*n2)/np.linalg.norm(y*n2,2)
        dve[2]=np.linalg.norm(x.reshape((1,-1))*n3)/np.linalg.norm(y*n3,2)                                                                                                                                                                                                                                                                                                                   
    return dve
#print dvec(u,tras)
#SN=[]
for u in U:
    #devc(u,tras)
    sn=np.linalg.norm(np.linalg.inv(chol)*u,2)   #信噪比
    #SN.append(sn)
    print u.reshape((1,-1)),' ',sn
   # print sn
    print '\n'

#print SN

