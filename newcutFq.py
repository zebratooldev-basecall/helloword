import sys
import os
import gzip
import time
start_time = time.time()
if len(sys.argv) != 5:
    print 'EXE: <fq.gz> <top/end> <finalLength> <rawlenth> '
    sys.exit()

def cutFq(fq, pos, length,raw):
    if not os.path.exists(fq):
        return

    fqAddr = os.path.dirname(fq)
    newfq = os.path.join(fqAddr, 'newtemp.fq') ####################
    fhOut = open(newfq, 'w')           ############################
    t1 = time.time()
    os.system('gzip -d %s'%(fq))       ############################
    t2 = time.time()
    print 'it cost %d seconds to decompression'%(t2 - t1)
    if pos == 'top':
        pass
    elif pos == 'end':
        pass
    else:
        print 'Wrong Parameter.\n'
        return
    '''
    with gzip.open(fq, 'r') as fhIn:
        idLine = 1
        while idLine:
            idLine = fhIn.readline().strip()
            seqLine = fhIn.readline().strip()
            plusLine = fhIn.readline().strip()
            qualityLine = fhIn.readline().strip()

            if not idLine:
                break

            string = ''
            string += idLine+'\n'
            if pos == 'top':
                string += seqLine[:length]+'\n'
            else:
                string += seqLine[-length:]+'\n'

            string += plusLine+'\n'

            if pos == 'top':
                string += qualityLine[:length]+'\n'
            else:
                string += qualityLine[-length:]+'\n'

            fhOut.write(string)

    fhIn.close()
    fhOut.close()
    '''
    name = fq[len(fqAddr)+1:-3]           ###########################
    fqn = os.path.join(fqAddr,name)       ###########################
    for line in open(fqn, 'rb'):
        line = line.strip()
        if len(line) == raw:
            if pos == 'top':
                str = line[:length] + '\n'
                fhOut.write(str)
            else:
                str = line[-length:] + '\n'
                fhOut.write(str)
        else:
            fhOut.write('%s\n'%(line))
    fhOut.close()
    t3 = time.time()
    os.system('gzip %s'%(fqn))
    os.system('gzip %s'%(newfq))
    t4 = time.time()
    print 'it cost %d seconds to compression'%(t4 - t3)

    
cutFq(sys.argv[1], sys.argv[2], int(sys.argv[3]),int(sys.argv[4]))

end_time = time.time()
print end_time - start_time

